terraform {
  backend gcs {
    bucket = "kubernetes-platform-demo-production-tfstate"
    prefix = "terraform/state"
  }
}